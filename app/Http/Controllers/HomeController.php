<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class HomeController extends Controller
{
    public function show(){
        $categories=Category::where('featured', 1)->get();
        $posts=Post::where('featured', 1)->get();
        return view('home', ['posts'=>$posts, 'categories'=>$categories]);
    }
}
