<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class ShopController extends Controller
{
    public function show(){
        $posts=Post::all();
        return view('shop', ['posts'=>$posts]);
    }

    public function single($id){
        $post=Post::find($id);
        $posts=Post::where('featured', 1)->get();
        return view('single', ['post' => $post, 'posts' => $posts]);
    }

    public function categorie($id){
        $categorie=Category::find($id);
        $posts=Post::where('category_id', $id)->get();
        return view('category', ['categorie' => $categorie, 'posts' => $posts]);
    }
}
