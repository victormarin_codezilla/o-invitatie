<?php

use App\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@show');

Route::get('/shop', 'ShopController@show'); 

Route::get('shop/product/{id}', 'ShopController@single');

Route::get('shop/categorie/{id}', 'ShopController@categorie');