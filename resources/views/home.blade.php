@extends('app')

@section('content')
    <div class="site-blocks-cover" style="background-image: url('img/banner.png'); margin:0 10px;" data-aos="fade">
      <div class="container">
        <div class="row">
          <div class="col-md-7 text-center text-md-left pt-md-0" style="margin-top:8%; color:white;">
            <h4>MAI SIMPLU SI MAI NOU</h4>
            <h1 class="mb-2" style="margin-top:7%; color:white">Invitatii digitale<br>pentru evenimentul tau</h1>
            <div class="intro-text text-center text-md-left">
              <p class="mb-4" style="margin-top:7%; color:white">Impresioneaza-ti prietenii cu o invitatie trimisa online!</p>
              <p style="margin-top:7%">
                <a href="#" class="btn-sm buton-banner">ALEGE MODELUL</a>
              </p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
    <div class="container" style="margin-top:-50px;">
    <div class="row" style="min-height:0px;">
          <div class="col-md-1"></div>
          <div class="col-md-10" style="background-color:#333333; padding:20px;">
            <div class="row">
              <div class="col-md-4">
                <img src="{{ asset('img/mail-icon.svg') }}" alt="" height="50px" style="float:left; margin-right:10px;">
                <h5 style="color:white">INVITATIE DIGITALA</h5>
                <h6 style="color:white; font-size:12px;">Simplu de trimis online</h6>
              </div>
              <div class="col-md-4">
              <img src="{{ asset('img/secure-icon.svg') }}" alt="" height="50px" style="float:left; margin-right:10px;">
                <h5 style="color:white">100% IN SIGURANTA</h5>
                <h6 style="color:white; font-size:12px;">Comanda online</h6>
              </div>
              <div class="col-md-4">
              <img src="{{ asset('img/123-icon.svg') }}" alt="" height="50px" style="float:left; margin-right:10px;">
                <h5 style="color:white">TREI PASI SIMPLI</h5>
                <h6 style="color:white; font-size:12px;">Alege, personalizeaza, comanda</h6>
              </div>
            </div>
          </div>
          <div class="col-md-1"></div>
        </div>
    </div>
    <div class="container" style="margin-top:50px">
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <div class="row">
          <div class="col-md-3" style="padding-left:10px; padding-right:10px">
          <img src="{{ asset('img/card1.png') }}" alt="" width="100%">
          <p style="text-align:center; margin-bottom:0px; margin-top:5px; font-size:12px; color:black">INVITATII</p>
          <p style="text-align:center; line-height:16px; color:black"><strong>NUNTA</strong></p>
        </div>
        <div class="col-md-3" style="padding-left:10px; padding-right:10px">
          <img src="{{ asset('img/card2.png') }}" alt="" width="100%">
          <p style="text-align:center; margin-bottom:0px; margin-top:5px; font-size:12px; color:black">INVITATII</p>
          <p style="text-align:center; line-height:16px; color:black"><strong>BOTEZ</strong></p>
        </div>
        <div class="col-md-3" style="padding-left:10px; padding-right:10px">
        <img src="{{ asset('img/card3.png') }}" alt="" width="100%">
          <p style="text-align:center; margin-bottom:0px; margin-top:5px; font-size:12px; color:black">INVITATII</p>
          <p style="text-align:center; line-height:16px; color:black"><strong>ANIVERSARE</strong></p>
        </div>
        <div class="col-md-3" style="padding-left:10px; padding-right:10px;">
        <img src="{{ asset('img/card4.png') }}" alt="" width="100%">
        <p style="text-align:center; margin-bottom:0px; margin-top:5px; font-size:12px; color:black">INVITATII</p>
          <p style="text-align:center; line-height:16px; color:black"><strong>CORPORATE</strong></p>
        </div>
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
    </div>
    <div class="container" style="margin-top:70px;">
      <div class="row">
        <div class="col-md-12">
          <h2 style="color:black; font-weight:normal; text-align:center">Cele mai populare invitatii</h2>
        </div>
      </div>
      <div class="row" style="margin-top:30px;">
        <div class="col-md-1"></div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-4" style="text-align:center">
              <img src="{{ asset('img/wed1.jpg') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">20 lei</p>
              </div>
              <div class="col-md-4" style="text-align:center">
              <img src="{{ asset('img/wed2.jpg') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
              </div>
              <div class="col-md-4" style="text-align:center">
              <img src="{{ asset('img/wed3.png') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
              </div>
            </div>
          </div>
        <div class="col-md-1"></div>
      </div>
    </div>

    <div class="site-blocks-cover" style="background-image: url('img/banner2.png'); margin:20px 10px;" data-aos="fade">
      <div class="container">
        <div class="row">
          <div class="col-md-7 text-center text-md-left pt-md-0" style="margin-top:8%;">
            <h1 style=" color:white">Simplu ca 1, 2, 3</h4>
            <div style="margin-top:7%">
            <img src="{{ asset('img/tick-icon.svg') }}" width="30px">
            <p class="mb-2" style="color:black; font-size:26px; display:inline">Alege design-ul</p>
            </div>
            <div style="margin-top:7%">
            <img src="{{ asset('img/tick-icon.svg') }}" width="30px">
            <p class="mb-2" style="color:black; font-size:26px; display:inline">Personalizeaza</p>
            </div>
            <div style="margin-top:7%">
            <img src="{{ asset('img/tick-icon.svg') }}" width="30px">
            <p class="mb-2" style="color:black; font-size:26px; display:inline">Trimite</p>
            </div>
            <div class="intro-text text-center text-md-left">
              <p style="margin-top:7%">
                <a href="#" class="btn-sm buton-banner2">INCEARCA ACUM</a>
              </p>
            </div>
          </div>
        </div>
        
      </div>
    </div>
@endsection