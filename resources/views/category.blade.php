@extends('app')

@section('content')
<div class="site-blocks-cover" style="background-image: url('../../img/banner3.png'); margin:0 10px; min-height:380px; height:380px !important;" data-aos="fade">
    <div class="container">
        <div class="row">
          <div class="col-md-6 text-center text-md-left pt-md-0" style="margin-top:2%;">
            
<h1 class="mb-2" style="margin-top:5%; color:#AA877D">Invitatii <?php if ($categorie->id<4) { ?>de<?php } ?> <span style="text-transform:lowercase">{{ $categorie->name }}</span></h1>
            <div class="intro-text text-center text-md-left">
              <p class="mb-4" style="margin-top:7%;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
              <div class="row" style="margin-top:7%">
                <div class="col-md-6">
                <img src="{{ asset('img/pencil-icon.svg') }}" alt="" height="40px" style="float:left; margin-right:20px;">
                <p style="line-height:20px;">Text<br>personalizabil</p>
                </div>
                <div class="col-md-6">
                <img src="{{ asset('img/cards-icon.svg') }}" alt="" height="40px" style="float:left; margin-right:20px;">
                <p style="line-height:20px;">75<br>modele</p>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-top: 30px;">
    <div class="row">
        <div class="col-md-3" style="padding-left:30px;">
            <h5 style="color:black;">Sortare dupa</h5>
            <ul class="sortare-dupa">
                <li class="active"><a href="#">Popularitate</a></li>
                <li><a href="#">Pret crescator</a></li>
                <li><a href="#">Pret descrescator</a></li>
                <li><a href="#">Data (cele mai noi)</a></li>
            </ul>
            <h5 style="color:black; margin-top:30px">Stilul invitatiei</h5>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Clasic
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Romantic
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Modern
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Traditional
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Lorem ipsum
                </label>
            </div>
            <h5 style="color:black; margin-top:30px">Forma</h5>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Vertical
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Patrat
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Orizontal
                </label>
            </div>
            <h5 style="color:black; margin-top:30px">Pret</h5>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    0-50 lei
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    50-150 lei
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    150-250 lei
                </label>
            </div>

            <h5 style="color:black; margin-top:30px; color:#AA877D;">Culoare</h5>
            <div style="margin-top:20px; display:block">
            <img src="{{ asset('img/color-1.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-2.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-3.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-4.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-5.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            </div>
            <div style="clear:both"></div>
            <div style="margin-top:10px; display:block">
            <img src="{{ asset('img/color-6.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-7.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-8.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-9.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-10.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            </div>
            <div style="clear:both"></div>
            <div style="margin-top:10px">
            <img src="{{ asset('img/color-11.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-12.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            <img src="{{ asset('img/color-13.svg') }}" alt="" height="35px" style="float:left; margin-right:10px;">
            </div>
            <div style="clear:both"></div>
        </div>
        <div class="col-md-9">
            <div class="row">
            <div class="col-md-4" style="text-align:center; margin-top:20px;">
              <img src="{{ asset('img/nunta1.png') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4); ">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">20 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center;  margin-top:20px;">
              <img src="{{ asset('img/nunta2.png') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center; margin-top:20px;">
              <img src="{{ asset('img/nunta3.png') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center;  margin-top:20px;">
              <img src="{{ asset('img/nunta4.jpg') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">20 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center;  margin-top:20px;">
              <img src="{{ asset('img/nunta5.jpg') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center;  margin-top:20px;">
              <img src="{{ asset('img/nunta6.png') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center; margin-top:20px;">
              <img src="{{ asset('img/nunta7.png') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">20 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center; margin-top:20px;">
              <img src="{{ asset('img/nunta8.jpg') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
            </div>
            <div class="col-md-4" style="text-align:center; margin-top:20px;">
              <img src="{{ asset('img/nunta9.jpg') }}" alt="" height="350px" style="box-shadow: 0px 0px 7px 1px rgba(136,136,136,0.4)">
              <p style="color:black; margin-top:10px; margin-bottom:0px; font-weight:bold;">Lorem ipsum dolor sit amet</p>
              <p style="color:black; line-height:16px;">25 lei</p>
            </div>
        </div>
    </div>
</div>
@endsection