<footer class="site-footer border-top">
      <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12" style="text-align:center">
            <img src="{{ asset('img/logo.png') }}" width="160px">
            <h6>Invitatii personalizate</h6>
            <br>
            <h6>&copy; 2020 oinvitatie.ro</h6>
            </div>
            
            <div class="col-lg-2 col-sm-4">
            <h5>Companie</h5>
            <ul class="meniu-footer">
              <li><a href="#">Despre noi</a></li>
              <li><a href="#">Proces de productie</a></li>
              <li><a href="#">Noutati</a></li>
              <li><a href="#">Cariere</a></li>
            </ul>
            </div>
            <div class="col-lg-2 col-sm-4">
            <h5>Informatii</h5>
            <ul class="meniu-footer">
              <li><a href="#">Cum comand</a></li>
              <li><a href="#">Intrebari frecvente</a></li>
              <li><a href="#">Politica de retur</a></li>
              <li><a href="#">Termeni si conditii</a></li>
            </ul>
            </div>
            <div class="col-lg-2 col-sm-4">
            <h5>Contul meu</h5>
            <ul class="meniu-footer">
            <li><a href="#">Informatii client</a></li>
              <li><a href="#">Comenzi</a></li>
              <li><a href="#">Favorite</a></li>
              <li><a href="#">Adrese</a></li>
            </ul>
            </div>
            <div class="col-lg-2 col-sm-4">
            <h5>Conecteaza-te</h5>
            <ul class="meniu-footer">
              <li>0721 123 456</li>
              <li>contact@oinvitatie.ro</li>
            </ul>
            <ul class="social-icons">
              <li><img src="{{ asset('img/skype-icon.svg') }}" width="35px" alt=""></li>
              <li><img src="{{ asset('img/twitter-icon.svg') }}" width="35px" alt=""></li>
              <li><img src="{{ asset('img/facebook-icon.svg') }}" width="35px" alt=""></li>
              <li><img src="{{ asset('img/linkedin-icon.svg') }}" width="35px" alt=""></li>
            </ul>
            </div>




          <!--
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">O invitatie</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Link 1</a></li>
                  <li><a href="#">Link 2</a></li>
                  <li><a href="#">Link 3</a></li>
                  <li><a href="#">Link 4</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Link 1</a></li>
                  <li><a href="#">Link 2</a></li>
                  <li><a href="#">Link 3</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Link 1</a></li>
                  <li><a href="#">Link 2</a></li>
                  <li><a href="#">Link 3</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
            <h3 class="footer-heading mb-4">Promo</h3>
            <a href="#" class="block-6">
              <img src="{{ asset('img/democard.png') }}" alt="Image placeholder" class="img-fluid rounded mb-4">
              <h3 class="font-weight-light  mb-0">Crafting Your Perfect Card</h3>
              <p>Promo from February 15 &mdash; 25, 2020</p>
            </a>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Contact Info</h3>
              <ul class="list-unstyled">
                <li class="address">aici trecem adresa</li>
                <li class="phone"><i class="fas fa-ad"></i> <a href="#">+40722xxxxxx</a></li>
                <li class="email">email@oinvitatie.ro</li>
              </ul>
            </div>

            <div class="block-7">
              <form action="#" method="post">
                <label for="email_subscribe" class="footer-heading">Subscribe</label>
                <div class="form-group">
                  <input type="text" class="form-control py-4" id="email_subscribe" placeholder="Email">
                  <input type="submit" class="btn btn-sm btn-primary" value="Trimite">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
            
            Copyright &copy; 2020 O invitatie | All rights reserved
            
            </p>
          </div>
          -->
        </div>
      </div>
    </footer>
  </div>

  <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script src="{{ asset('js/jquery-ui.js') }}"></script>
  <script src="{{ asset('js/popper.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('js/aos.js') }}"></script>

  <script src="{{ asset('js/main.js') }}"></script>