<div class="site-wrap">
    <header class="site-navbar" role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
              <form action="" class="site-block-top-search">
                <span class="icon"><img src="{{ asset('img/search-icon.svg') }}" width="20px"></span>
                <input type="text" style="border:none;" class="form-control border-1" placeholder="Căutare...">
              </form>
            </div>

            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="{{ url('/') }}" class="js-logo-clone" style="border:none"><img src="{{ asset('img/logo.png') }}" width="200px"></a>
              </div>
            </div>

            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <li><a href="#" style="margin-right:20px;"><img src="{{ asset('img/cart-icon.svg') }}" width="30px"></a></li>
                  <li><a href="#" style="margin-right:0;">Login</a> |  <a href="#">Cont nou <img src="{{ asset('img/user-icon.svg') }}" width="20px"></a></li>
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <!--<li class="has-children">-->
          
              
              <!--<ul class="dropdown">
                <li><a href="#">Menu One</a></li>
                <li><a href="#">Menu Two</a></li>
                <li><a href="#">Menu Three</a></li>
                <li class="has-children">
                  <a href="#">Sub Menu</a>
                  <ul class="dropdown">
                    <li><a href="#">Menu One</a></li>
                    <li><a href="#">Menu Two</a></li>
                    <li><a href="#">Menu Three</a></li>
                  </ul>
                </li>
              </ul>-->

              <!--
                <li class="has-children">
              <a href="">NUNTA</a>
              <ul class="dropdown">
                <li><a href="#">Subcategorie 1</a></li>
                <li><a href="#">Subcategorie 2</a></li>
                <li><a href="#">Subcategorie 3</a></li>
              </ul>
            </li>
                -->
            
            <li><a href="{{ url('/shop/categorie/1') }}">NUNTA</a></li>
            <li><a href="{{ url('/shop/categorie/3') }}">BOTEZ</a></li>
            <li><a href="{{ url('/shop/categorie/4') }}">ANIVERSARE</a></li>
            <li><a href="{{ url('/shop/categorie/6') }}">CORPORATE</a></li>
            
          </ul>
        </div>
      </nav>
    </header>

    

    

    